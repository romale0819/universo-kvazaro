// Плагин аутентификации
/* eslint no-console: ["error", { allow: ["warn", "error"] }] */

const login_query = `
mutation($login: String!, $password: String!) {
  ensaluti(login: $login, password: $password) {
    status
    message
    konfirmita
  }
}
`

const logout_query = `
mutation {
  elsaluti {
    status
    message
  }
}
`

const user_query = `
query {
  mi {
    uuid
    objId
    unuaNomo {
      enhavo
    }
    familinomo {
      enhavo
    }
    avataro {
      bildoE {
        url
      }
    }
    universoUzanto {
      retnomo
    }
    konfirmita
  }
}
`


// устанавливаем атрибут $auth
export default ({ app, store, Vue }) => {
  const _auth = {
    async login(login, password) {
      // Мутация автроризации
      return Vue.prototype.$axios.post('/registrado/', {
        query: login_query,
        variables: {
          login: login,
          password: password
        }
      })
        .then(response => {
          // Извлекаем результат мутации
          const resp = response.data.data.ensaluti

          if (resp.status) {
            // Успешная авторизация
            return Promise.resolve()
          }

          // Неуспешная авторизация
          store.commit('siriusoauth/setKonfirmita', resp.konfirmita)
          return Promise.reject(new Error(resp.message))
        })
        .catch(e => {
          console.error(e)
          // Ошибка запроса на уровне сети/протокола
          return Promise.reject(e)
        })
    },

    async logout() {
      // Мутация выхода
      return Vue.prototype.$axios.post('/registrado/', {query: logout_query})
        .then(response => {
          // Извлекаем результат мутации
          const resp = response.data.data.elsaluti

          if (resp.status) {
            // Успешное завершение
            return Promise.resolve()
          }

          // Неуспешное завершение
          return Promise.reject(new Error(resp.message))
        })
        .catch(e => {
          console.error(e)
          // Ошибка запроса на уровне сети/протокола
          return Promise.reject(e)
        })
    },

    async fetchUser() {
      // Извлечение данных авторизованного пользователя
      return Vue.prototype.$axios.post('/registrado/', {query: user_query})
        .then(async response => {
          // Извлекаем результат мутации
          const mi = response.data.data.mi

          if (mi) {
            // Если получили данные пользователя
            // сохраняем данные в хранилище,
            // устанавливаем признак авторизации в True
            store.commit('siriusoauth/setUser', mi)
            store.commit('siriusoauth/setIsLoggedIn', true)
            store.commit('siriusoauth/setKonfirmita', mi.konfirmita)

            return Promise.resolve()
          }

          // Иначе данные пользователя отсутствуют,
          // обнуляем сохранённые данные,
          // устанавливаем признак авторизации в False
          store.commit('siriusoauth/setUser', null)
          store.commit('siriusoauth/setIsLoggedIn', false)
          return Promise.resolve()
        })
        .catch(async e => {
          console.error(e)
          // Если ошибка протокола/сети,
          // то считаем, что авторизации нет,
          // сбрасываем данные пользователя
          // и признак авторизации
          store.commit('siriusoauth/setUser', null)
          store.commit('siriusoauth/setIsLoggedIn', false)
          store.commit('siriusoauth/setKonfirmita', null)
          return Promise.reject(e)
        })
    },

    get isLoggedIn() {return store.getters["siriusoauth/isLoggedIn"]},
    get loggedIn() {return store.getters["siriusoauth/isLoggedIn"]},
    get user() {return store.getters["siriusoauth/user"]}
  }

  Vue.prototype.$auth = _auth
}
