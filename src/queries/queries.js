import gql from "graphql-tag";
//запрос для получения списка пользователей
export const uzantojQuery = gql`
  query Uzantoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: uzantoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    uzantoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
//запрос для получения данных пользователя по objID
export const uzantoByObjIdQuery = gql`
  query Uzanto($id: Float!) {
    uzanto: uzantoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
//запрос для получения списка товарищей
export const gekamaradojQuery = gql`
  query Gekamaradoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: gekamaradoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    gekamaradoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
// Запрос для получения списка всех сообществ
export const komunumojQuery = gql`
  query Komunumoj(
    $first: Int = 1
    $after: String
    $enDato: DateTime
    $serchi: String
    $tipo: String
    $forigo: Boolean = false
  ) {
    lasto: komunumoj(last: 1, tipo_Kodo: $tipo) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    komunumoj(
      first: $first
      after: $after
      enDato: $enDato
      serchi: $serchi
      tipo_Kodo_In: $tipo
      orderBy: ["-rating", "-aktiva_dato"]
      forigo: $forigo
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
          statistiko {
            tuta
          }
        }
      }
    }
  }
`;
// Запрос для получения данных конкретного сообщества по objID
export const komunumoQuery = gql`
  query Komunumo($id: Float = 13) {
    komunumo: komunumoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          tipo {
            kodo
            nomo {
              enhavo
            }
          }
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
          informo {
            enhavo
          }
          informoBildo {
            bildo
            bildoMaks
          }
          kontaktuloj {
            edges {
              node {
                objId
                unuaNomo {
                  enhavo
                }
                duaNomo {
                  enhavo
                }
                familinomo {
                  enhavo
                }
                avataro {
                  bildoF {
                    url
                  }
                }
                kontaktaInformo
              }
            }
          }
          rajtoj
        }
      }
    }
  }
`;
//запрос для получения списка проектов сообщества по ID сообщества
export const projektojQuery = gql`
  query Projektoj(
    $komunumoId: Float
    $first: Int = 25
    $forigo: Boolean = false
    $arkivo: Boolean = false
  ) {
    projektoj: universoProjektojProjekto(
      first: $first
      universoprojektojprojektoposedanto_PosedantoKomunumo_Id: $komunumoId
      arkivo: $arkivo
      forigo: $forigo
    ) {
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          universoprojektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                id
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;
//запрос для получения данных проекта по UUID. После реализации в бакенде поменяем на ID
export const projektoByUuidQuery = gql`
  query Projekto($uuid: UUID, $objId: Float) {
    projekto: universoProjektojProjekto(uuid: $uuid, objId: $objId) {
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          universoprojektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                uuid
                id
                objId
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;
//типы сообществ
export const komunumojTipojQuery = gql`
  query komunumojTipoj {
    komunumojTipoj {
      edges {
        node {
          nomo {
            enhavo
          }
          kodo
          kvantoKomomunumoj
        }
      }
    }
  }
`;
//категории проектов
export const universoProjektojProjektoKategorioQuery = gql`
  query universoProjektojProjektoKategorio {
    kategorio: universoProjektojProjektoKategorio {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//типы проектов
export const universoProjektojProjektoTipoQuery = gql`
  query universoProjektojProjektoTipoQuery {
    tipo: universoProjektojProjektoTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//статусы проектов
export const universoProjektojProjektoStatusoQuery = gql`
  query universoProjektojProjektoStatusoQuery {
    statuso: universoProjektojProjektoStatuso {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//информация о текущем пользователе
export const mi = gql`
  query Mi {
    mi {
      id
      objId
      uuid
      unuaNomo {
        enhavo
      }
      duaNomo {
        enhavo
      }
      familinomo {
        enhavo
      }
      sekso
      konfirmita
      isActive
      chefaLingvo {
        id
        nomo
      }
      chefaTelefonanumero
      chefaRetposhto
      naskighdato
      loghlando {
        nomo {
          lingvo
          enhavo
          chefaVarianto
        }
        id
      }
      loghregiono {
        id
        nomo {
          enhavo
        }
      }
      agordoj
      avataro {
        id
        bildoE {
          url
        }
      }
      statuso {
        enhavo
      }
      statistiko {
        miaGekamarado
        miaGekamaradoPeto
        kandidatoGekamarado
        tutaGekamaradoj
        rating
        aktivaDato
      }
      kontaktaInformo
      kandidatojTuta
      gekamaradojTuta
      chefaOrganizo {
        id
        nomo {
          enhavo
        }
      }
      sovetoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      sindikatoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      administritajKomunumoj {
        edges {
          node {
            id
            nomo {
              enhavo
            }
          }
        }
      }
      universoUzanto {
        id
        uuid
        retnomo
      }
    }
  }
`;
//запрос для получения списка всех проектов
export const tutaProjektojQuery = gql`
  query tutaProjektoj(
    $first: Int = 25
    $after: String
    $forigo: Boolean = false
    $arkivo: Boolean = false
  ) {
    tutaProjektoj: universoProjektojProjekto(
      first: $first
      after: $after
      arkivo: $arkivo
      forigo: $forigo
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          id
          objId
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          universoprojektojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                id
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;
//типы задач
export const universoProjektojTaskoTipoQuery = gql`
  query universoProjektojTaskoTipoQuery {
    tipo: universoProjektojTaskoTipo {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//категории задач
export const universoProjektojTaskoKategorioQuery = gql`
  query universoProjektojTaskoKategorioQuery {
    kategorio: universoProjektojTaskoKategorio {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
//статусы задач
export const universoProjektojTaskoStatusoQuery = gql`
  query universoProjektojTaskoStatusoQuery {
    statuso: universoProjektojTaskoStatuso {
      edges {
        node {
          nomo {
            enhavo
          }
          uuid
          id
          objId
        }
      }
    }
  }
`;
