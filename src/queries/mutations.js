import gql from "graphql-tag";
//добавление проекта
export const addProjekto = gql`
  mutation addProjekto(
    $publikigo: Boolean = true
    $nomo: String!
    $kategorio: [Int] = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $priskribo: String!
    $posedantoUzantoId: Int!
  ) {
    redaktuUniversoProjektojProjekto(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      posedantoUzantoId: $posedantoUzantoId
    ) {
      status
      message
      universoProjekto {
        uuid
        objId
      }
    }
  }
`;
//регистрация пользователя Универсо
export const addUniversoUzanto = gql`
  mutation($retnomo: String!) {
    redaktuUniversoUzanto(retnomo: $retnomo, publikigo: true) {
      status
      message
    }
  }
`;
//удаление проекта
export const forigoProjekto = gql`
  mutation forigoProjekto($id: UUID, $forigo: Boolean = true) {
    redaktuUniversoProjektojProjekto(uuid: $id, forigo: $forigo) {
      status
      message
    }
  }
`;
//архивация проекта
export const arkProjekto = gql`
  mutation arkProjekto($id: UUID, $arkivo: Boolean = true) {
    redaktuUniversoProjektojProjekto(uuid: $id, arkivo: $arkivo) {
      status
      message
    }
  }
`;
//добавление задачи
export const addTasko = gql`
  mutation addTasko(
    $publikigo: Boolean = true
    $nomo: String!
    $kategorio: [Int] = 1
    $tipoId: Int = 1
    $statusoId: Int = 1
    $priskribo: String!
    $projektoUuid: String
  ) {
    redaktuUniversoProjektojTaskoj(
      publikigo: $publikigo
      nomo: $nomo
      kategorio: $kategorio
      tipoId: $tipoId
      statusoId: $statusoId
      priskribo: $priskribo
      projektoUuid: $projektoUuid
    ) {
      status
      message
      universoTaskoj {
        uuid
        id
        objId
      }
    }
  }
`;
//создание категории задач
export const addTaskojKategorio = gql`
  mutation addTaskojKategorio($publikigo: Boolean = true, $nomo: String!, $realeco: [Int] = 1) {
    redaktuUniversoProjektojTaskojKategorio(publikigo: $publikigo, nomo: $nomo, realeco: $realeco) {
      status
      message
      universoTaskojKategorioj {
        uuid
        id
        objId
      }
    }
  }
`;
//создание типов задач
export const addTaskojTipo = gql`
  mutation addTaskojTipo($publikigo: Boolean = true, $nomo: String!, $realeco: [Int] = 1) {
    redaktuUniversoProjektojTaskojTipo(publikigo: $publikigo, nomo: $nomo, realeco: $realeco) {
      status
      message
      universoTaskojTipoj {
        uuid
        id
        objId
      }
    }
  }
`;
//создание статусов задач
export const addTaskojStatuso = gql`
  mutation addTaskojStatuso($publikigo: Boolean = true, $nomo: String!) {
    redaktuUniversoProjektojTaskoStatuso(publikigo: $publikigo, nomo: $nomo) {
      status
      message
      universoTaskojStatusoj {
        uuid
        id
        objId
      }
    }
  }
`;
//архивация задачи
export const arkTasko = gql`
  mutation arkTasko($id: UUID, $arkivo: Boolean = true) {
    redaktuUniversoProjektojTaskoj(uuid: $id, arkivo: $arkivo) {
      status
      message
    }
  }
`;
//удаление задачи
export const forigoTasko = gql`
  mutation arkTasko($id: UUID, $forigo: Boolean = true) {
    redaktuUniversoProjektojTaskoj(uuid: $id, forigo: $forigo) {
      status
      message
    }
  }
`;
